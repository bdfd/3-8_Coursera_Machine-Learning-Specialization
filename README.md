# 3.0_Coursera_Course-NAME
This Specialization from leading researchers at the University of Washington introduces you to the exciting, high-demand field of Machine Learning. Through a series of practical case studies, you will gain applied experience in major areas of Machine Learning including Prediction, Classification, Clustering, and Information Retrieval. You will learn to analyze large and complex datasets, create systems that adapt and improve over time, and build intelligent applications that can make predictions from data.

1. C1-Machine Learning Foundations: A Case Study Approach
2. C2-Machine Learning: Regression
3. C3-Machine Learning: Classification
4. C4-Machine Learning: Clustering & Retrieval

